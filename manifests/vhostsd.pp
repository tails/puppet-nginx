# Deploy vhosts.d files for nginx
define nginy::vhostsd(
  $ensure  = 'present',
  $source  = ["puppet:///modules/site_nginx/${::fqdn}/vhosts.d/${name}.conf",
              "puppet:///modules/site_nginx/vhosts.d/${name}.conf" ],
  $content = false,
){
  file{"/etc/nginx/vhosts.d/${name}.conf":
    ensure => $ensure,
    notify => Service['nginx'],
  }

  if $ensure == 'present' {
    File["/etc/nginx/vhosts.d/${name}.conf"]{
      owner => root,
      group => 0,
      mode  => '0644'
    }
    if $content {
      File["/etc/nginx/vhosts.d/${name}.conf"]{
        content => $content
      }
    } else {
      File["/etc/nginx/vhosts.d/${name}.conf"]{
        source => $source
      }
    }
  }
}
