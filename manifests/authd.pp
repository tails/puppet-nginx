# Deploy auth.d files for nginx
define nginy::authd(
  $ensure  = 'present',
  $source  = ["puppet:///modules/site_nginx/${::fqdn}/auth.d/htpasswd.${name}",
              "puppet:///modules/site_nginx/auth.d/htpasswd.${name}",
              "puppet:///modules/nginy/auth.d/htpasswd.${name}"],
  $content = false,
){
  file{"/etc/nginx/auth.d/htpasswd.${name}":
    ensure  => $ensure,
  }

  if $ensure == 'present' {
    File["/etc/nginx/auth.d/htpasswd.${name}"]{
      owner   => root,
      group   => 0,
      mode    => '0644'
    }
    if $content {
      File["/etc/nginx/auth.d/htpasswd.${name}"]{
        content => $content
      }
    } else {
      File["/etc/nginx/auth.d/htpasswd.${name}"]{
        source => $source
      }
    }
  }
}
