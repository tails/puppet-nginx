class nginy::access_log(
    $mode = 'distribution'
){

    nginy::confd{ 'access_log':
        source => [ "puppet:///modules/nginy/access_log/${mode}/${::operatingsystem}_${::lsbdistcodename}.conf",
                    "puppet:///modules/nginy/access_log/${mode}/${::operatingsystem}.conf",
                    "puppet:///modules/nginy/access_log/${mode}/fallback.conf",
                  ]
    }

}
