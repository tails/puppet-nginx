define nginy::conf(
  $ensure = 'present',
  $source = '',
  $content = undef )
{

  if $source == '' and $content == undef {
    fail("One of \$source or \$content must be specified for nginy::conf ${name}")
  }

  if $source != '' and $content != undef {
    fail("Only one of \$source or \$content must specified for nginy::conf ${name}")
  }

  notice('nginy::conf is deprecated, please migrate to nginy::confd')

  nginy::confd { $name:
    ensure  => $ensure,
    source  => $source,
    content => $content,
  }

}
