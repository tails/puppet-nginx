# manage nginx stuff
# Copyright (C) 2007 admin@immerda.ch
# GPLv3
#
#  Parameters:
#
#    * manage_shorewall_http:  Open http port with shorewall?
#    * manage_shorewall_https: Open https port with shorewall?
#
class nginy (
  $custom_config = false,
  $access_log             = "distribution",
  $error_log              = "distribution",
  $ensure                 = running,
  $enable                 = true,
  $manage_shorewall_http  = false,
  $manage_shorewall_https = false,
  $use_munin              = false,
  $vhosts                 = {},
  $confs                  = {},
) {

  # Validate some parameters.
  case $access_log {
    'distribution',
    'noip',
    'none':  {}
    default: {
      fail('The access_log parameter can only be one of: distribution, noip, none')
    }
  }
  case $error_log {
    'distribution',
    'none':  {}
    default: {
      fail('The error_log parameter can only be one of: distribution, none')
    }
  }

  include ::nginy::base

  if $manage_shorewall_http {
    include ::shorewall::rules::http
  }
  if $manage_shorewall_https {
    include ::shorewall::rules::https
  }

  if $use_munin {
    include ::nginy::munin
  } else {
    include ::nginy::munin::disable
  }

  if str2bool($::selinux) {
    include ::nginy::selinux
  }

  create_resources('nginy::vhostsd',$vhosts)
  create_resources('nginy::confd',$confs)
}
