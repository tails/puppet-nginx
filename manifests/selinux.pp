# manage selinux bits
class nginy::selinux {
  selboolean{
    'httpd_setrlimit':
      value      => 'on',
      persistent => true,
  }
}
