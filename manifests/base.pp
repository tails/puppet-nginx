# base installation
class nginy::base {
  package{'nginx':
    ensure => installed,
  }

  file{ 'nginx_config':
    path    => '/etc/nginx/nginx.conf',
    owner   => root,
    group   => 0,
    mode    => '0644',
    require => Package[nginx],
    notify  => Service[nginx],
  }

  # Template data
  $error_log = $nginy::error_log

  if $nginy::custom_config {
    File['nginx_config'] {
      source => [
        "puppet:///modules/site_nginx/${::fqdn}/nginx.conf",
        'puppet:///modules/site_nginx/nginx.conf',
        'puppet:///modules/nginy/nginx.conf'
      ],
    }
  } else {
    File['nginx_config'] {
      content => $::lsbdistcodename ? {
        ''      => template("nginy/nginx.conf/${::operatingsystem}.erb"),
        default => template("nginy/nginx.conf/${::operatingsystem}_${::lsbdistcodename}.erb"),
      },
    }
  }

  file{ 'nginx_configd':
    path    => '/etc/nginx/conf.d',
    owner   => root,
    group   => 0,
    mode    => '0755',
    require => Package[nginx],
    notify  => Service[nginx],
  }

  file{ '/etc/nginx/includes':
    ensure  => link,
    target  => '/etc/nginx/include.d',
    force   => true,
    require => Package[nginx],
    notify  => Service[nginx],
  }

  class {'nginy::access_log': mode => $nginy::access_log }

  file{
    ['/etc/nginx/auth.d','/etc/nginx/include.d','/etc/nginx/vhosts.d' ]:
      ensure  => directory,
      owner   => root,
      group   => 0,
      mode    => '0644',
      recurse => true,
      purge   => true,
      force   => true,
      require => Package['nginx'],
      notify  => Service['nginx'];
  }

  file{ '/etc/nginx/conf.d/virtual.conf':
    ensure  => file,
    owner   => root,
    group   => 0,
    mode    => '0644',
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  file_line{'load_nginx_vhosts':
    line    => 'include /etc/nginx/vhosts.d/*.conf;',
    path    => '/etc/nginx/conf.d/virtual.conf',
    require => Package['nginx'],
    notify  => Service['nginx'];
  }

  $included_snippets = [
    'deny-access-to-git',
    'deny-access-to-ht_files',
    'ssl-strong-ciphers',
    'sts',
  ]
  nginy::included { $included_snippets: }

  service{'nginx':
    require => Package[nginx],
  } ->  exec{'reload_nginx':
    refreshonly => true,
    command     => '/usr/bin/systemctl reload nginx',
  }

  if $nginy::ensure != 'none' {
    Service['nginx'] {
      ensure => $nginy::ensure,
    }
  }

  if $nginy::enable != 'none' {
    Service['nginx'] {
      enable => $nginy::enable,
    }
  }

}
